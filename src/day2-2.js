import { getInput } from './helpers'
import { IntcodeProgram } from './day2'
export default async function() {
  const program = new IntcodeProgram(await getInput('day2'))
  for (var a = 0; a <= 99; a++) {
    for (var b = 0; b <= 99; b++) {
      program.reset()
      program.setValue(1, a)
      program.setValue(2, b)
      if (program.run() === 19690720) {
        return 100 * a + b
      }
    }
  }
}
