import { getInput } from './helpers'
export default async function() {
  const input = await getInput('day1')
  const fuel = f => Math.floor(f / 3) - 2
  const result = input
    .match(/[^\r\n]+/g)
    .map(s => {
      let n = fuel(Number(s))
      let f = n
      while (f > 0) {
        f = fuel(f)
        if (f > 0) n += f
      }
      return n
    })
    .reduce((a, c) => a + (Math.floor(c / 3) - 2), 0)
  return result
}

// Browser version:
// const fuel = f => Math.floor(f / 3) - 2
// fetch('https://adventofcode.com/2019/day/1/input')
//   .then(data => data.text())
//   .then(input => input.match(/[^\r\n]+/g))
//   .then(strs =>
//     strs.map(s => {
//       let n = fuel(Number(s))
//       let f = n
//       while (f > 0) {
//         f = fuel(f)
//         if (f > 0) n += f
//       }
//       return n
//     })
//   )
//   .then(nums => nums.reduce((a, c) => a + c, 0))
//   .then(console.log)
