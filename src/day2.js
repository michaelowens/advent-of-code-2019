import { getInput } from './helpers'

export class IntcodeProgram {
  constructor(ops) {
    this.input = ops.split(',').map(Number)
    this.ops = this.input.slice()
    this.pos = 0
  }

  setValue(position, value) {
    this.ops[position] = value
  }

  reset() {
    this.pos = 0
    this.ops = this.input.slice()
  }

  run() {
    for (; this.pos < this.ops.length; this.pos += 4) {
      switch (this.ops[this.pos]) {
        case 1:
          this.ops[this.ops[this.pos + 3]] =
            this.ops[this.ops[this.pos + 1]] + this.ops[this.ops[this.pos + 2]]
          break
        case 2:
          this.ops[this.ops[this.pos + 3]] =
            this.ops[this.ops[this.pos + 1]] * this.ops[this.ops[this.pos + 2]]
          break
        case 99:
          return this.ops[0]
      }
    }
  }
}

export default async function() {
  const program = new IntcodeProgram(await getInput('day2'))
  program.setValue(1, 12)
  program.setValue(2, 2)
  return program.run()
}
