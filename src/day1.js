import { getInput } from './helpers'
export default async function() {
  const input = await getInput('day1', true)
  const result = input
    .map(Number)
    .reduce((a, c) => a + (Math.floor(c / 3) - 2), 0)
  return result
}

// Browser version:
// fetch('https://adventofcode.com/2019/day/1/input')
//   .then(data => data.text())
//   .then(input => input.match(/[^\r\n]+/g))
//   .then(strs => strs.map(Number))
//   .then(nums => nums.reduce((a, c) => a + (Math.floor(c / 3) - 2), 0))
//   .then(console.log)
