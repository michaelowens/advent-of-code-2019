import { getInput } from './helpers'

const abs = Math.abs
export const getDistance = (a, b) =>
  abs(abs(a[0]) + abs(a[1]) - (abs(b[0]) + abs(b[1])))

export const intersects = (line1, line2) => {
  const denominator =
    (line2.to[1] - line2.from[1]) * (line1.to[0] - line1.from[0]) -
    (line2.to[0] - line2.from[0]) * (line1.to[1] - line1.from[1])
  if (denominator == 0) {
    return false
  }

  let a = line1.from[1] - line2.from[1]
  let b = line1.from[0] - line2.from[0]
  const numerator1 =
    (line2.to[0] - line2.from[0]) * a - (line2.to[1] - line2.from[1]) * b
  const numerator2 =
    (line1.to[0] - line1.from[0]) * a - (line1.to[1] - line1.from[1]) * b
  a = numerator1 / denominator
  b = numerator2 / denominator

  const x = Math.round(line1.from[0] + a * (line1.to[0] - line1.from[0]))
  const y = Math.round(line1.from[1] + a * (line1.to[1] - line1.from[1]))

  if (a > 0 && a < 1 && b > 0 && b < 1) {
    return [x, y]
  }

  return false
}

export const findIntersections = (wire1, wire2) => {
  let intersections = []
  const step = wire1.step()
  for (let line of step) {
    const step2 = wire2.step()
    for (let line2 of step2) {
      let int = intersects(line, line2)
      if (int !== false) {
        intersections.push(int)
      }
    }
  }
  return intersections
}

export class Wire {
  constructor(instructions) {
    this.instructions = instructions.split(',')
    this.lines = []
    this.pos = [0, 0]
  }

  *step() {
    for (let i = 0; i < this.instructions.length; i++) {
      if (i > this.lines.length - 1) {
        this.saveStep(this.instructions[i])
      }
      yield this.lines[i]
    }
  }

  saveStep(instruction) {
    const dir = instruction.substr(0, 1)
    const val = Number(instruction.substring(1))
    const c = dir === 'U' || dir === 'D' ? 1 : 0
    const isInc = dir === 'D' || dir === 'R'
    const oldPos = this.pos.slice()
    this.pos[c] = isInc ? this.pos[c] + val : this.pos[c] - val
    this.lines.push({
      from: oldPos,
      to: this.pos.slice(),
    })
  }
}

export default async function() {
  const input = await getInput('day3', true)
  const wire1 = new Wire(input[0])
  const wire2 = new Wire(input[1])
  const intersections = findIntersections(wire1, wire2)
  let shortestDistance = Infinity
  for (let p of intersections) {
    const d = getDistance([0, 0], p)
    if (d < shortestDistance) {
      shortestDistance = d
    }
  }
  return shortestDistance
}
