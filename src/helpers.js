import fs from 'fs'
import path from 'path'

export const getInput = async (fileName, perLine) =>
  new Promise((resolve, reject) => {
    fs.readFile(path.join('input', fileName + '.txt'), 'utf8', (err, data) => {
      if (err) throw err
      resolve(perLine ? data.match(/[^\r\n]+/g) : data)
    })
  })
