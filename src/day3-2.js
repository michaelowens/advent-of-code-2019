import { getInput } from './helpers'
import { Wire, intersects } from './day3'

export const findIntersections = (wire1, wire2) => {
  let intersections = []
  const step = wire1.step()
  let stepCount1 = 0
  let stepCount2 = 0
  for (let line of step) {
    const step2 = wire2.step()
    stepCount2 = 0
    for (let line2 of step2) {
      let int = intersects(line, line2)
      if (int !== false) {
        const k = line.from[0] === line.to[0] ? 1 : 0
        const k2 = line2.from[0] === line2.to[0] ? 1 : 0
        const distance = Math.abs(line.from[k] - int[k])
        const distance2 = Math.abs(line2.from[k2] - int[k2])
        intersections.push({
          pos: int,
          steps: stepCount1 + distance + stepCount2 + distance2,
        })
      }

      stepCount2 += Math.abs(line2.to[0] - line2.from[0])
      stepCount2 += Math.abs(line2.to[1] - line2.from[1])
    }

    stepCount1 += Math.abs(line.to[0] - line.from[0])
    stepCount1 += Math.abs(line.to[1] - line.from[1])
  }
  return intersections
}

export default async function() {
  const input = await getInput('day3', true)
  const wire1 = new Wire(input[0])
  const wire2 = new Wire(input[1])
  const intersections = findIntersections(wire1, wire2)
  let shortestDistance = Infinity
  for (let int of intersections) {
    const d = int.steps
    if (d < shortestDistance) {
      shortestDistance = d
    }
  }
  return shortestDistance
}
