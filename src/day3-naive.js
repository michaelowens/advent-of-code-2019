/**
 * This version is extremely naive, it makes a 2D array with every position a wire would pass
 * Extremely inefficient
 */

import { getInput } from './helpers'
import { getDistance } from './day3'
export class Wire {
  constructor(instructions) {
    this.instructions = instructions.split(',')
    this.positions = []
    this.x = 0
    this.y = 0

    for (let i = 0; i < this.instructions.length; i++) {
      this.goTo(this.instructions[i])
    }
  }

  goTo(instruction) {
    const dir = instruction.substr(0, 1)
    const val = Number(instruction.substring(1))
    for (let i = 1; i <= val; i++) {
      const isX = dir === 'U' || dir === 'D'
      const isInc = dir === 'D' || dir === 'R'
      const c = isX ? 'x' : 'y'
      this[c] = isInc ? this[c] + 1 : this[c] - 1
      this.positions.push([this.x, this.y])
    }
  }

  findIntersections(wire) {
    return this.positions.filter(p => wire.contains(p))
  }

  contains(p) {
    return (
      this.positions.findIndex(pos => pos[0] === p[0] && pos[1] === p[1]) !== -1
    )
  }
}
export default async function() {
  // const input = await getInput('day3', true)
  const input = [
    'R75,D30,R83,U83,L12,D49,R71,U7,L72',
    'U62,R66,U55,R34,D71,R55,D58,R83',
  ]
  const wire1 = new Wire(input[0])
  const wire2 = new Wire(input[1])
  const intersections = wire1.findIntersections(wire2)
  console.log(intersections)
  let shortestDistance = Infinity
  for (let p of intersections) {
    const d = getDistance([0, 0], p)
    if (d < shortestDistance) {
      shortestDistance = d
    }
  }

  return shortestDistance
}
