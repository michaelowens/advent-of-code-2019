require('@babel/polyfill')
require('@babel/register')
const path = require('path')

if (process.argv.length < 3) {
  console.log('Usage exampe: npm start day1')
  process.exit()
}

;(async function() {
  console.log('Running:', process.argv[2])
  const result = await require('./' +
    path.join('src', process.argv[2])).default()
  console.log('Result:', result)
})()
