require('@babel/polyfill')
require('@babel/register')
const path = require('path')

const tests = [
  ['day1', 3147032],
  ['day1-2', 1572333],
  ['day2', 4090701],
  ['day2-2', 6421],
  // ['day3-naive', 1337],
  ['day3', 1337],
  ['day3-2', 65356],
]

const test = async puzzle => {
  const [filename, expected] = tests.find(t => t[0] === puzzle)
  process.stdout.write(`Testing ${filename}... `)
  const start = process.hrtime()
  const result = await require('./' + path.join('src', filename)).default()
  const end = process.hrtime(start)
  console.log(
    result === expected ? 'PASSED' : 'FAILED',
    `(${end[0]}s ${end[1] / 1000000}ms)`
  )
  if (result !== expected) {
    console.log('Expected', expected, 'but got', result)
  }
}

;(async function() {
  if (process.argv.length > 2) {
    test(process.argv[2])
    return
  }

  for (let i = 0; i < tests.length; i++) {
    await test(tests[i][0])
  }
})()
