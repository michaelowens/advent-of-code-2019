# Advent of Code 2019

Some puzzles may include multiple solutions. Solutions are not intended to be as short as possible, but rather as "implementations" that could be used as in the context of the puzzles.

```shell
$ npm i # Install babel
$ npm start day1 # Run a single puzzle
$ npm test
```
